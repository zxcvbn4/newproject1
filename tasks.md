
## пример JSON
```json
[
    {
      "name": "Война и мир",
      "author": "Лев Толстой",
      "yearOfPublication": 1869,
      "genre": "Роман-эпопея",
      "language": "Русский"
       },
    {
      "name": "Гарри Поттер и философский камень",
      "author": "J.K. Rowling",
      "yearOfPublication": 1997,
      "genre": "Фэнтези",
      "language": "Английский"
       },
    {
      "name": "Атлант расправил плечи",
      "author": "Айн Рэнд",
      "yearOfPublication": 1957,
      "genre": "Роман-антиутопия",
      "language": "Английский"
      }
  ]
```  
## пример YAML
```yaml
- name: "Война и мир"
  author: "Лев Толстой"
  yearOfPublication: 1869
  genre: "Роман-эпопея"
  language: "Русский"
- name: "Гарри Поттер и философский камень"
  author: "J.K. Rowling"
  yearOfPublication: 1997
  genre: "Фэнтези"
  language: "Английский"
- name: "Атлант расправил плечи"
  author: "Айн Рэнд"
  yearOfPublication: 1957
  genre: "Роман-антиутопия"
  language: "Английский"
```


[Документация JSON](https://json-schema.org/specification)
[Документация YAML](https://yaml.org/spec/1.2.2/)




| Тип данных   | Описание                                              | Пример использования                                      |
|--------------|-------------------------------------------------------|-----------------------------------------------------------|
| String       | Строковый тип данных. Используется для хранения текста.| `"name": "Maria"`                                      |
| Number       | Числовой тип данных. Используется для хранения чисел. | `"age": 21`                                               |
| Boolean      | Логический тип данных. Используется для хранения значений true/false. | `"isStudent": true`                               |
| Object       | Тип данных объекта. Используется для хранения набора пар "ключ-значение". | `"adress": {"street": "Greenstreet", "house": 16}`                   |
| Array        | Тип данных массива. Используется для хранения упорядоченного списка элементов. | `"hobbies": ["reading", "painting", "gardening"]`         |
| Null         | Тип данных, представляющий пустое значение.          | `"Patronymic": null`                                      |





